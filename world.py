import resources
from network_object import *
import pyglet
from pyglet.window import key
import math
import cPickle
import messages
import random
from noise import snoise2, pnoise2

TICKS_PER_SEC = 60
GRAVITY = 20.0
MAX_JUMP_HEIGHT = 1.0 # About the height of a block.
# To derive the formula for calculating jump speed, first solve
#    v_t = v_0 + a * t
# for the time at which you achieve maximum height, where a is the acceleration
# due to gravity and v_t = 0. This gives:
#    t = - v_0 / a
# Use t and the desired MAX_JUMP_HEIGHT to solve for v_0 (jump speed) in
#    s = s_0 + v_0 * t + (a * t^2) / 2
JUMP_SPEED = math.sqrt(2 * GRAVITY * MAX_JUMP_HEIGHT)
WALKING_SPEED = 5
FLYING_SPEED = 15
TERMINAL_VELOCITY = 50
PLAYER_HEIGHT = 2

class WorldTile(NetworkObject):
    def __init__(self, batch=None, material='water',id=0):
        super(WorldTile, self).__init__()
        self.batch = batch
        self.sprites = []
        self.transitions = []
        self.position = (0, 0, 0)
        self.flying = False
        self.id = str(id)
        self.has_change = False
        self.blocking = False
        self.material = material

    def to_string(self):
        return str(self.position)

    def update_position(self):
        super(WorldTile, self).update_position()
        x, y, z = self.position
        for sprite in self.sprites:
            sprite.x = x*64
            sprite.y = y*64

    def add_img(self,index):
        if self.material == 'dirt':
            img = resources.grass
            self.sprites.append(pyglet.sprite.Sprite(img=img, batch=self.batch, subpixel=True, group=resources.background))
            img = resources.dirt_images[index]
            self.sprites.append(pyglet.sprite.Sprite(img=img, batch=self.batch, subpixel=True, group=resources.foreground))
        if self.material == 'grass':
            img = resources.grass
            self.sprites.append(pyglet.sprite.Sprite(img=img, batch=self.batch, subpixel=True))

    def same_material(self, material):
        if material == self.material:
            return 1
        return 0

class World(object):
    def __init__(self,batch=None):
        self.batch = batch
        self.size = (32,32)
        self.tiles = {}

    def initialize(self):
        xsize, ysize = self.size
        octaves = 4
        freq = 1*octaves
        for x in range(-xsize,xsize):
            row = ''
            for y in range(-ysize,ysize):
                number = int(snoise2(x / freq, y / freq, octaves)*50)+50
                #row += str(number)
                if number > 50:
                    material = 'dirt'
                    row += '#'
                else:
                    material = 'grass'
                    row += '-'
                #row +=','
                tile = WorldTile(self.batch,material=material,id=str(x)+'_'+str(y))
                tile.position = (x,y,0)
                self.add_tile((x,y),tile)
            print row
        for x in range(-xsize+1,xsize-1):
            for y in range(-ysize+1,ysize-1):
                #create sprites for world
                mat = self.tiles[(x,y)].material
                index = 0
                index += self.tiles[(x,y+1)].same_material(mat)*1
                index += self.tiles[(x+1,y)].same_material(mat)*2
                index += self.tiles[(x,y-1)].same_material(mat)*4
                index += self.tiles[(x-1,y)].same_material(mat)*8
                self.tiles[(x,y)].add_img(index)
                self.tiles[(x,y)].update_position()

    def add_transitions(self):
        xsize, ysize = self.size
        for x in range(-xsize,xsize):
            for y in range(-ysize,ysize):
                image = random.choice([resources.dirt_texture, resources.grass_texture])
                tile = WorldTile(self.batch,img=image,id=str(x)+'_'+str(y))
                tile.position = (x,y,0)
                tile.update_position()
                self.add_tile((x,y),tile)

    def add_tile(self, position, tile, immediate=True):
        self.tiles[position] = tile

