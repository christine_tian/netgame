import legume
import pyglet
import utils
import math
from pyglet.window import key, mouse
from pyglet import image
from pyglet.gl import *
from player import *
import legume
import sys, getopt
import messages
import resources
import world

TICKS_PER_SEC = 60
GRAVITY = 20.0
MAX_JUMP_HEIGHT = 1.0 # About the height of a block.
# To derive the formula for calculating jump speed, first solve
#    v_t = v_0 + a * t
# for the time at which you achieve maximum height, where a is the acceleration
# due to gravity and v_t = 0. This gives:
#    t = - v_0 / a
# Use t and the desired MAX_JUMP_HEIGHT to solve for v_0 (jump speed) in
#    s = s_0 + v_0 * t + (a * t^2) / 2
JUMP_SPEED = math.sqrt(2 * GRAVITY * MAX_JUMP_HEIGHT)
WALKING_SPEED = 100
FLYING_SPEED = 15
TERMINAL_VELOCITY = 50
PLAYER_HEIGHT = 2
SERVER = True
ZOOM_IN_FACTOR = 1.2
ZOOM_OUT_FACTOR = 1/ZOOM_IN_FACTOR

class Window(pyglet.window.Window):

    def __init__(self, *args, **kwargs):
        super(Window, self).__init__(*args, **kwargs)

        # Whether or not the window exclusively captures the mouse.
        self.exclusive = True

        # When flying gravity has no effect and speed is increased.
        self.flying = False

        # Strafing is moving lateral to the direction you are facing,
        # e.g. moving to the left or right while continuing to face forward.
        #
        # First element is -1 when moving forward, 1 when moving back, and 0
        # otherwise. The second element is -1 when moving left, 1 when moving
        # right, and 0 otherwise.
        self.strafe = [0, 0]

        self.batch = pyglet.graphics.Batch()
        # Current (x, y, z) position in the world, specified with floats. Note
        # that, perhaps unlike in math class, the y-axis is the vertical axis.
        self.position = (0, 0, 0)
        self.player_batch = pyglet.graphics.Batch()

        # First element is rotation of the player in the x-z plane (ground
        # plane) measured from the z-axis down. The second is the rotation
        # angle from the ground plane up. Rotation is in degrees.
        #
        # The vertical plane rotation ranges from -90 (looking straight down) to
        # 90 (looking straight up). The horizontal rotation range is unbounded.
        self.rotation = (0, 0)

        # Which sector the player is currently in.
        self.sector = None

        # The crosshairs at the center of the screen.
        self.reticle = None

        # Velocity in the y (upward) direction.
        self.dy = 0

        self.objects={}

        self.server = legume.Server()
        self.server.OnConnectRequest += self.join_request_handler
        self.server.OnMessage += self.server_message_handler

        self.client = legume.Client()
        self.client.OnConnectRequestAccepted += self.on_connected
        self.client.OnMessage += self.client_message_handler

        # Convenience list of num keys.
        self.num_keys = [
            key._1, key._2, key._3, key._4, key._5,
            key._6, key._7, key._8, key._9, key._0]

        # The label that is displayed in the top left of the canvas.
        self.label = pyglet.text.Label('', font_name='Arial', font_size=18,
            x=10, y=self.height - 10, anchor_x='left', anchor_y='top',
            color=(0, 0, 0, 255))

        # This call schedules the `update()` method to be called
        # TICKS_PER_SEC. This is the main game event loop.
        self.widgets = []
        self.focus = None
        self.text_cursor = self.get_system_mouse_cursor('text')
        self.PLAYER_NAME = 'Player'
        self.SERVER_PORT = 10000
        self.SERVER_HOSTNAME = 'Localhost'
        self.left   = 0
        self.right  = self.width
        self.bottom = 0
        self.top    = self.height
        self.zoom_level = 1
        self.zoomed_width  = self.width
        self.zoomed_height =  self.height


        pyglet.clock.schedule_interval(self.update, 1.0 / TICKS_PER_SEC)

    def update(self, dt):
        for widget in self.widgets:
            if widget.to_delete:
                widget.delete()
                self.widgets.remove(widget)
        if self.start_server_button.clicked:
            self.SERVER_HOSTNAME = self.widgets[1].document.text.split(':')[0]
            self.SERVER_PORT = self.widgets[1].document.text.split(':')[1]
            self.start_server_button.clicked = False
            for widget in self.widgets:
                widget.to_delete = True
            self.start_server()
        if self.start_client_button.clicked:
            self.SERVER_HOSTNAME = self.widgets[1].document.text.split(':')[0]
            self.SERVER_PORT = self.widgets[1].document.text.split(':')[1]
            self.PLAYER_NAME = self.widgets[0].document.text
            self.start_client()
            self.start_client_button.clicked = False
            for widget in self.widgets:
                widget.to_delete = True
        for k,v in self.objects.items():
            v.update(dt)

    def join_request_handler(self, sender, args):
        # send current game state
        print 'Client joining'
        message = messages.NextPlayerID()
        message.o_id.value = len(self.objects)
        print message
        sender.send_reliable_message(message)

    def on_connected(self, sender, args):
        #create and send player
        print 'Connected to server'
        sender.send_reliable_message(self.player.join_message())
        #message = messages.PlayerJoin()
        #message.o_id = len(self.objects)
        #self.client.send_reliable_message(message)


    def client_message_handler(self, sender, msg):
        # process a message
        if msg.MessageTypeID == messages.NextPlayerID.MessageTypeID:
            ##spawn player with id
            player = Player(self.player_batch,msg.o_id.value,str(self.PLAYER_NAME))
            player.client = self.client
            self.objects[player.id] = player
            self.push_handlers(player)
            self.player = player
        if msg.MessageTypeID == messages.ObjectUpdate.MessageTypeID:
            try:
                self.objects[msg.o_id.value].position = (msg.x.value, msg.y.value, msg.z.value)
            except KeyError:
                # add create object code here!
                obj = globals()[msg.o_type.value](self.batch, msg.o_id.value)
                obj.position = (msg.x.value, msg.y.value, msg.z.value)
                self.objects[obj.id] = obj
                pass

    def server_message_handler(self, sender, msg):
        # process a message
        if msg.MessageTypeID == messages.PlayerJoin.MessageTypeID:
            #player joined ok!
            player = Player(self.player_batch,id=msg.o_id.value,name=msg.name.value,)
            player.position = (msg.x.value, msg.y.value, 0)
            player.id = msg.o_id.value
            print player.name + ' Joined'
            self.objects[player.id] = player
            self.player = player
        if msg.MessageTypeID == messages.PlayerKeyRelease.MessageTypeID:
            self.objects[msg.o_id.value]._on_key_release(msg.symbol.value, msg.modifiers.value)
        if msg.MessageTypeID == messages.PlayerKeyPress.MessageTypeID:
            self.objects[msg.o_id.value]._on_key_press(msg.symbol.value, msg.modifiers.value)

    def send_updates(self):
        #for ball in self._balls.itervalues():
        #    logging.debug('**** sending update for ball # %s' % ball.ball_id)
        #    print('Sending update for ball # %s' % ball.ball_id)
        #    self.server.send_reliable_message_to_all(ball.get_message())
        for k,v in self.objects.items():
            #print v.get_update_message().o_id.value
            self.server.send_reliable_message_to_all(v.get_update_message())

    def start_server(self):
        self.server.listen(('', int(self.SERVER_PORT)))
        print('Listening on port %s' % self.SERVER_PORT)
        self.world = world.World(batch=self.batch)
        self.world.initialize()
        pyglet.clock.schedule_interval(self.server_update, 1.0 / TICKS_PER_SEC)

    def start_client(self):
        self.client.connect((self.SERVER_HOSTNAME, int(self.SERVER_PORT)))
        print('Connecting to %s:%s' % (self.SERVER_HOSTNAME,self.SERVER_PORT))
        pyglet.clock.schedule_interval(self.client_update, 1.0 / TICKS_PER_SEC)

    def server_update(self, dt):
        self.server.update()
        if self.server.peercount > 0:
            self.send_updates()

    def client_update(self, dt):
        self.client.update()

    def set_exclusive_mouse(self, exclusive):
        """ If `exclusive` is True, the game will capture the mouse, if False
        the game will ignore the mouse.

        """
        super(Window, self).set_exclusive_mouse(exclusive)
        self.exclusive = exclusive

    def get_sight_vector(self):
        """ Returns the current line of sight vector indicating the direction
        the player is looking.

        """
        x, y = self.rotation
        # y ranges from -90 to 90, or -pi/2 to pi/2, so m ranges from 0 to 1 and
        # is 1 when looking ahead parallel to the ground and 0 when looking
        # straight up or down.
        m = math.cos(math.radians(y))
        # dy ranges from -1 to 1 and is -1 when looking straight down and 1 when
        # looking straight up.
        dy = math.sin(math.radians(y))
        dx = math.cos(math.radians(x - 90)) * m
        dz = math.sin(math.radians(x - 90)) * m
        return (dx, dy, dz)

    def get_motion_vector(self):
        """ Returns the current motion vector indicating the velocity of the
        player.

        Returns
        -------
        vector : tuple of len 3
            Tuple containing the velocity in x, y, and z respectively.

        """
        if any(self.strafe):
            x, y = self.rotation
            strafe = math.degrees(math.atan2(*self.strafe))
            y_angle = math.radians(y)
            x_angle = math.radians(x + strafe)
            if self.flying:
                m = math.cos(y_angle)
                dy = math.sin(y_angle)
                if self.strafe[1]:
                    # Moving left or right.
                    dy = 0.0
                    m = 1
                if self.strafe[0] > 0:
                    # Moving backwards.
                    dy *= -1
                # When you are flying up or down, you have less left and right
                # motion.
                dx = math.cos(x_angle) * m
                dz = math.sin(x_angle) * m
            else:
                dy = 0.0
                dx = math.cos(x_angle)
                dz = math.sin(x_angle)
        else:
            dy = 0.0
            dx = 0.0
            dz = 0.0
        return (dx, dy, dz)

    def on_mouse_press(self, x, y, button, modifiers):
        """ Called when a mouse button is pressed. See pyglet docs for button
        amd modifier mappings.

        Parameters
        ----------
        x, y : int
            The coordinates of the mouse click. Always center of the screen if
            the mouse is captured.
        button : int
            Number representing mouse button that was clicked. 1 = left button,
            4 = right button.
        modifiers : int
            Number representing any modifying keys that were pressed when the
            mouse button was clicked.

        """
        #if self.exclusive:
        #    pass
        #else:
        #    self.set_exclusive_mouse(True)

        for widget in self.widgets:
            if widget.hit_test(x, y):

                if hasattr(widget, 'clicked'):
                    widget.on_clicked()
                else:
                    self.set_focus(widget)
                break
        else:
            self.set_focus(None)

        if self.focus:
            if hasattr(self.focus,'caret'):
                self.focus.caret.on_mouse_press(x, y, button, modifiers)

    def on_mouse_motion(self, x, y, dx, dy):
        """ Called when the player moves the mouse.

        Parameters
        ----------
        x, y : int
            The coordinates of the mouse click. Always center of the screen if
            the mouse is captured.
        dx, dy : float
            The movement of the mouse.

        """
        if self.exclusive:
            m = 0.15
            x, y = self.rotation
            x, y = x + dx * m, y + dy * m
            y = max(-90, min(90, y))
            self.rotation = (x, y)

        for widget in self.widgets:
            if widget.hit_test(x, y):
                self.set_mouse_cursor(self.text_cursor)
                break
        else:
            self.set_mouse_cursor(None)

    def on_key_press(self, symbol, modifiers):
        """ Called when the player presses a key. See pyglet docs for key
        mappings.

        Parameters
        ----------
        symbol : int
            Number representing the key that was pressed.
        modifiers : int
            Number representing any modifying keys that were pressed.

        """

        if symbol == key.ESCAPE:
            self.set_exclusive_mouse(False)
        if symbol == key.ENTER:
            self.set_focus(None)
        if symbol == pyglet.window.key.TAB:
            if modifiers & pyglet.window.key.MOD_SHIFT:
                dir = -1
            else:
                dir = 1

            if self.focus in self.widgets:
                i = self.widgets.index(self.focus)
            else:
                i = 0
                dir = 0

            self.set_focus(self.widgets[(i + dir) % len(self.widgets)])

    def on_key_release(self, symbol, modifiers):
        """ Called when the player releases a key. See pyglet docs for key
        mappings.

        Parameters
        ----------
        symbol : int
            Number representing the key that was pressed.
        modifiers : int
            Number representing any modifying keys that were pressed.

        """
        pass

    def on_resize(self, width, height):
        """ Called when the window is resized to a new `width` and `height`.

        """
        # label
        super(Window, self).on_resize(width, height)
        #glViewport( 0, 0, width, height )
        self.label.y = height - 10
        # reticle
        if self.reticle:
            self.reticle.delete()
        x, y = self.width / 2, self.height / 2
        n = 10
        self.reticle = pyglet.graphics.vertex_list(4,
            ('v2i', (x - n, y, x + n, y, x, y - n, x, y + n))
        )
        for widget in self.widgets:
            widget.on_resize(width, height)
        self.right = self.left + self.width*self.zoom_level
        self.top = self.bottom + self.height*self.zoom_level
        self.zoomed_width = self.width*self.zoom_level
        self.zoomed_height = self.height*self.zoom_level

    def set_2d(self):
        """ Configure OpenGL to draw in 2d.

        """
        width, height = self.get_size()
        glDisable(GL_DEPTH_TEST)
        #glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        #glOrtho(0, width, 0, height, -1, 1)
        #glOrtho(0, width*self.zoom_level, 0, height*self.zoom_level, -1, 1)
        glOrtho(self.left, self.right, self.bottom, self.top, -1, 1)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()


    def set_3d(self):
        """ Configure OpenGL to draw in 3d.

        """
        width, height = self.get_size()
        glEnable(GL_DEPTH_TEST)
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(65.0, width / float(height), 0.1, 60.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        #x, y = self.rotation
        #glRotatef(x, 0, 1, 0)
        #glRotatef(-y, math.cos(math.radians(x)), 0, math.sin(math.radians(x)))
        #x, y, z = self.position
        #glTranslatef(-x, -y, -z)
        if hasattr(self,'player'):
            x, y, z = self.player.position
            glTranslatef(-x, -y, -z)

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        # Move camera
        self.left   -= dx*self.zoom_level
        self.right  -= dx*self.zoom_level
        self.bottom -= dy*self.zoom_level
        self.top    -= dy*self.zoom_level
        if self.focus:
            self.focus.caret.on_mouse_drag(x, y, dx, dy, buttons, modifiers)

    def on_mouse_scroll(self, x, y, dx, dy):
        # Get scale factor
        f = ZOOM_IN_FACTOR if dy > 0 else ZOOM_OUT_FACTOR if dy < 0 else 1
        # If zoom_level is in the proper range
        if .2 < self.zoom_level*f < 5:

            self.zoom_level *= f

            #mouse_x = float(x)/self.width
            #mouse_y = float(y)/self.height
            if hasattr(self,'player'):
                px, py, pz = player.position
                mouse_x = px/self.width
                mouse_y = py/self.height
            else:
                mouse_x = float(x)/self.width
                mouse_y = float(y)/self.height

            mouse_x_in_world = self.left   + mouse_x*self.zoomed_width
            mouse_y_in_world = self.bottom + mouse_y*self.zoomed_height

            self.zoomed_width  *= f
            self.zoomed_height *= f

            self.left   = mouse_x_in_world - mouse_x*self.zoomed_width
            self.right  = mouse_x_in_world + (1 - mouse_x)*self.zoomed_width
            self.bottom = mouse_y_in_world - mouse_y*self.zoomed_height
            self.top    = mouse_y_in_world + (1 - mouse_y)*self.zoomed_height

    def on_draw(self):
        """ Called by pyglet to draw the canvas.

        """
        self.clear()
        #self.set_3d()
        #glColor3d(1, 1, 1)
        #self.model.batch.draw()
        #self.draw_focused_block()
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        if hasattr(self,'player'):
            x, y, z = self.player.position
            glTranslatef(-x, -y, -z)
        self.batch.draw()
        self.set_2d()
        self.player_batch.draw()
        self.draw_label()
        self.draw_reticle()


    def draw_focused_block(self):
        """ Draw black edges around the block that is currently under the
        crosshairs.

        """
        vector = self.get_sight_vector()
        block = self.model.hit_test(self.position, vector)[0]
        if block:
            x, y, z = block
            vertex_data = cube_vertices(x, y, z, 0.51)
            glColor3d(0, 0, 0)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
            pyglet.graphics.draw(24, GL_QUADS, ('v3f/static', vertex_data))
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    def draw_label(self):
        """ Draw the label in the top left of the screen.

        """
        x, y, z = self.position
        self.label.text = '%02d (%.2f, %.2f, %.2f) %d / %d' % (
            pyglet.clock.get_fps(), x, y, z,
            1, 1)
        self.label.draw()

    def draw_reticle(self):
        """ Draw the crosshairs in the center of the screen.

        """
        glColor3d(0, 0, 0)
        self.reticle.draw(GL_LINES)

    def on_text(self, text):
        if self.focus:
            self.focus.caret.on_text(text)

    def on_text_motion(self, motion):
        if self.focus:
            self.focus.caret.on_text_motion(motion)

    def on_text_motion_select(self, motion):
        if self.focus:
            self.focus.caret.on_text_motion_select(motion)

    """
    def on_key_press(self, symbol, modifiers):
        if symbol == pyglet.window.key.TAB:
            if modifiers & pyglet.window.key.MOD_SHIFT:
                dir = -1
            else:
                dir = 1

            if self.focus in self.widgets:
                i = self.widgets.index(self.focus)
            else:
                i = 0
                dir = 0

            self.set_focus(self.widgets[(i + dir) % len(self.widgets)])
    """

    def set_focus(self, focus):
        if self.focus:
            if hasattr(self.focus,'caret'):
                self.focus.caret.visible = False
                self.focus.caret.mark = self.focus.caret.position = 0

        self.focus = focus
        if self.focus:
            if hasattr(self.focus,'caret'):
                self.focus.caret.visible = True
                self.focus.caret.mark = 0
                self.focus.caret.position = len(self.focus.document.text)

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:],"r:p:",["remote=","port="])
    except getopt.GetoptError:
        print 'main.py -r <remote_server> -p <port>'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-r", "--remote"):
            SERVER_HOSTNAME = arg
            SERVER = False
        elif opt in ("-p", "--port"):
            SERVER_PORT = arg
            SERVER = False
    window = Window(width=800, height=600, caption='Test', resizable=True)
    window.widgets.append(resources.TextWidget('Player', 100, window.height-100, window.width-200, window.batch))
    window.widgets.append(resources.TextWidget('Localhost:10000', 100, window.height-200, window.width-200, window.batch))
    window.start_server_button = resources.ButtonWidget(resources.start_server, 100, window.height - 400, window.batch)
    window.widgets.append(window.start_server_button)
    window.start_client_button = resources.ButtonWidget(resources.start_client, 400, window.height - 400, window.batch)
    window.widgets.append(window.start_client_button)
    pyglet.app.run()
