import legume

class PlayerJoin(legume.messages.BaseMessage):
    MessageTypeID = 100
    MessageValues = {
        'o_id' : 'string 32',
        'name': 'string 32',
        'x' : 'float',
        'y' : 'float',
    }

class NextPlayerID(legume.messages.BaseMessage):
    MessageTypeID = 110
    MessageValues = {
        'entity' : 'int',
    }

class PlayerKeyPress(legume.messages.BaseMessage):
    MessageTypeID = 120
    MessageValues = {
        'o_id' : 'string 32',
        'symbol' : 'int',
        'modifiers': 'int',
        'o_type': 'string 32',
    }
class PlayerKeyRelease(legume.messages.BaseMessage):
    MessageTypeID = 121
    MessageValues = {
        'o_id' : 'string 32',
        'symbol' : 'int',
        'modifiers': 'int',
    }

class PlayerKey(legume.messages.BaseMessage):
    MessageTypeID = 122
    MessageValues = {
        'entity' : 'int',
        'keycode' : 'string 32',
        'key_type': 'string 32',
    }

class CreateEntity(legume.messages.BaseMessage):
    MessageTypeID = 123
    MessageValues = {
        'entity' : 'int',
        'load_order' : 'varstring',
        'position' : 'varstring',
        'rotate' : 'varstring',
        'rotate_render' : 'varstring',
        'cymunk_physics' : 'varstring',
    }

class UpdateEntity(legume.messages.BaseMessage):
    MessageTypeID = 124
    MessageValues = {
        'entity' : 'int',
        'x' : 'float',
        'y' : 'float',
        'vx': 'float',
        'vy': 'float',
        #'r': 'float',
    }

class ObjectUpdate(legume.messages.BaseMessage):
    MessageTypeID = 130
    MessageValues = {
        'o_id' : 'string 32',
        'x': 'float',
        'y': 'float',
        'z': 'float',
        'o_type': 'string 32',
    }

legume.messages.message_factory.add(NextPlayerID)
legume.messages.message_factory.add(PlayerJoin)
legume.messages.message_factory.add(PlayerKeyPress)
legume.messages.message_factory.add(PlayerKey)
legume.messages.message_factory.add(PlayerKeyRelease)
legume.messages.message_factory.add(ObjectUpdate)
legume.messages.message_factory.add(CreateEntity)
legume.messages.message_factory.add(UpdateEntity)
