import json

def entity_to_dict(entity):
    entity_dict = {'id': entity.entity_id}
    #for system in entity.load_order:
    #   component = getattr(self.player, system)

    #    if system == 'position':
    #        pass

    #    if system == 'cymunk_physics':

    if hasattr(entity, 'load_order'):
        entity_dict['load_order'] = entity.load_order

    if hasattr(entity, 'position'):
        entity_dict['position'] = (entity.position.x, entity.position.y)

    if hasattr(entity, 'rotate'):
        entity_dict['rotate'] = {'r': entity.rotate.r }

    if hasattr(entity, 'rotate_renderer'):
        entity_dict['rotate_renderer'] = {'texture': entity.rotate_renderer.texture_key,'size': (entity.rotate_renderer.width, entity.rotate_renderer.height),'render': entity.rotate_renderer.render}

    if hasattr(entity, 'renderer'):
        entity_dict['renderer'] = {'texture': entity.renderer.texture_key,'size': (entity.renderer.width, entity.renderer.height),'render': entity.renderer.render}

    if hasattr(entity, 'cymunk_physics'):
        bd = {'velocity': (entity.cymunk_physics.body.velocity.x, entity.cymunk_physics.body.velocity.y),
				  'position': (entity.cymunk_physics.body.position.x, entity.cymunk_physics.body.position.y),
				  'angle': entity.cymunk_physics.body.angle,
				  'angular_velocity': entity.cymunk_physics.body.angular_velocity,
				  'vel_limit': entity.cymunk_physics.body.velocity_limit,
				  'ang_vel_limit': entity.cymunk_physics.body.angular_velocity_limit,
				  'mass': entity.cymunk_physics.body.mass
			}
        shapes = []
        for s in entity.cymunk_physics.shapes:
            if s.__class__.__name__ == 'Circle':
                shape_dict = {'inner_radius': 0, 'outer_radius': s.radius, 'mass': s.body.mass, 'offset': (s.offset.x, s.offset.y)}
            if s.__class__.__name__ == 'BoxShape':
                shape_dict = {'width': 50, 'height': 50, 'mass': s.body.mass}
            sd = {'collision_type': s.collision_type, 'elasticity': s.elasticity, 'friction': s.friction, 'shape_type': s.__class__.__name__, 'shape_info': shape_dict}
            shapes.append(sd)
        entity_dict['cymunk_physics'] = {'shapes':shapes, 'shape_type': entity.cymunk_physics.shape_type, 'body': bd}

    return entity_dict

    #        component_order = ['position', 'rotate', 'rotate_renderer',
    #            'cymunk_physics','movement']
