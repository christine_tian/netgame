import resources
from network_object import *
import pyglet
from pyglet.window import key
import math
import cPickle
import messages

TICKS_PER_SEC = 60
GRAVITY = 20.0
MAX_JUMP_HEIGHT = 1.0 # About the height of a block.
# To derive the formula for calculating jump speed, first solve
#    v_t = v_0 + a * t
# for the time at which you achieve maximum height, where a is the acceleration
# due to gravity and v_t = 0. This gives:
#    t = - v_0 / a
# Use t and the desired MAX_JUMP_HEIGHT to solve for v_0 (jump speed) in
#    s = s_0 + v_0 * t + (a * t^2) / 2
JUMP_SPEED = math.sqrt(2 * GRAVITY * MAX_JUMP_HEIGHT)
WALKING_SPEED = 100
FLYING_SPEED = 15
TERMINAL_VELOCITY = 50
PLAYER_HEIGHT = 2

class Player(NetworkObject):
    def __init__(self, batch=None, id=0, name=None ):
        super(Player, self).__init__()
        self.batch = batch

        self.player = pyglet.sprite.Sprite(img=resources.player_image, x=400, y=300, batch=self.batch)
        self.position = (0, 0, 0)
        self.strafe = [0, 0]
        self.rotation = (0, 0)
        self.dz = 0
        self.flying = False
        self.id = str(id)
        self.has_change = False
        self.name = name

    def to_string(self):
        return str(self.position) + str(self.rotation)

    def update_position(self):
        super(Player, self).update_position()
        x, y, z = self.position
        #self.player.x = x
        #self.player.y = y
        #self.player.z = z

    def collide(self, position, height):
        """ Checks to see if the player at the given `position` and `height`
        is colliding with any blocks in the world.

        Parameters
        ----------
        position : tuple of len 3
            The (x, y, z) position to check for collisions at.
        height : int or float
            The height of the player.

        Returns
        -------
        position : tuple of len 3
            The new position of the player taking into account collisions.

        """
        # How much overlap with a dimension of a surrounding block you need to
        # have to count as a collision. If 0, touching terrain at all counts as
        # a collision. If .49, you sink into the ground, as if walking through
        # tall grass. If >= .5, you'll fall through the ground.
        pad = 0.25
        p = list(position)
        """
        np = normalize(position)
        for face in FACES:  # check all surrounding blocks
            for i in xrange(3):  # check each dimension independently
                if not face[i]:
                    continue
                # How much overlap you have with this dimension.
                d = (p[i] - np[i]) * face[i]
                if d < pad:
                    continue
                for dy in xrange(height):  # check each height
                    op = list(np)
                    op[1] -= dy
                    op[i] += face[i]
                    if tuple(op) not in self.model.world:
                        continue
                    p[i] -= (d - pad) * face[i]
                    if face == (0, -1, 0) or face == (0, 1, 0):
                        # You are colliding with the ground or ceiling, so stop
                        # falling / rising.
                        self.dy = 0
                    break
                    """
        return tuple(p)

    def get_motion_vector(self):
        """ Returns the current motion vector indicating the velocity of the
        player.

        Returns
        -------
        vector : tuple of len 3
            Tuple containing the velocity in x, y, and z respectively.

        """
        if any(self.strafe):
            x, z = self.rotation
            strafe = math.degrees(math.atan2(*self.strafe))
            z_angle = math.radians(z)
            x_angle = math.radians(x + strafe)
            if self.flying:
                m = math.cos(z_angle)
                dz = math.sin(z_angle)
                if self.strafe[1]:
                    # Moving left or right.
                    dz = 0.0
                    m = 1
                if self.strafe[0] > 0:
                    # Moving backwards.
                    dz *= -1
                # When you are flying up or down, you have less left and right
                # motion.
                dx = math.cos(x_angle) * m
                dy = math.sin(x_angle) * m
            else:
                dx = math.cos(x_angle)
                dy = math.sin(x_angle)
                dz = 0.0
        else:
            dx = 0.0
            dy = 0.0
            dz = 0.0
        return (dx, dy, dz)

    def on_key_press(self, symbol, modifiers):
        """ Called when the player presses a key. See pyglet docs for key
        mappings.

        Parameters
        ----------
        symbol : int
            Number representing the key that was pressed.
        modifiers : int
            Number representing any modifying keys that were pressed.

        """
        message = messages.PlayerKeyPress()
        message.o_id.value = self.id
        message.symbol.value = symbol
        message.modifiers.value = modifiers
        self.client.send_reliable_message(message)
        print 'press sent'


    def _on_key_press(self, symbol, modifiers):
        """ Called when the player presses a key. See pyglet docs for key
        mappings.

        Parameters
        ----------
        symbol : int
            Number representing the key that was pressed.
        modifiers : int
            Number representing any modifying keys that were pressed.

        """
        if symbol == key.W:
            self.strafe[0] += 1
        elif symbol == key.S:
            self.strafe[0] -= 1
        elif symbol == key.A:
            self.strafe[1] -= 1
        elif symbol == key.D:
            self.strafe[1] += 1
        elif symbol == key.SPACE:
            if self.dy == 0:
                self.dy = JUMP_SPEED
        elif symbol == key.TAB:
            self.flying = not self.flying

    def on_key_release(self, symbol, modifiers):
        """ Called when the player releases a key. See pyglet docs for key
        mappings.

        Parameters
        ----------
        symbol : int
            Number representing the key that was pressed.
        modifiers : int
            Number representing any modifying keys that were pressed.

        """
        message = messages.PlayerKeyRelease()
        message.o_id.value = self.id
        message.symbol.value = symbol
        message.modifiers.value = modifiers
        self.client.send_reliable_message(message)
        print 'release sent'

    def _on_key_release(self, symbol, modifiers):
        """ Called when the player releases a key. See pyglet docs for key
        mappings.

        Parameters
        ----------
        symbol : int
            Number representing the key that was pressed.
        modifiers : int
            Number representing any modifying keys that were pressed.

        """
        if symbol == key.W:
            self.strafe[0] -= 1
        elif symbol == key.S:
            self.strafe[0] += 1
        elif symbol == key.A:
            self.strafe[1] += 1
        elif symbol == key.D:
            self.strafe[1] -= 1

    def update(self, dt):
        """ This method is scheduled to be called repeatedly by the pyglet
        clock.

        Parameters
        ----------
        dt : float
            The change in time since the last call.

        """
        super(Player, self).update(dt)
        temp = self.to_string()
        #self.model.process_queue()

        m = 8
        dt = min(dt, 0.2)
        for _ in xrange(m):
            self._update(dt / m)
        if temp != self.to_string():
            self.has_change = True

    def _update(self, dt):
        """ Private implementation of the `update()` method. This is where most
        of the motion logic lives, along with gravity and collision detection.

        Parameters
        ----------
        dt : float
            The change in time since the last call.

        """
        # walking
        speed = FLYING_SPEED if self.flying else WALKING_SPEED
        d = dt * speed # distance covered this tick.
        dx, dy, dz = self.get_motion_vector()
        # New position in space, before accounting for gravity.
        dx, dy, dz = dx * d, dy * d, dz * d
        # gravity
        if not self.flying:
            # Update your vertical speed: if you are falling, speed up until you
            # hit terminal velocity; if you are jumping, slow down until you
            # start falling.
            #self.dz -= dt * GRAVITY
            #self.dz = max(self.dz, -TERMINAL_VELOCITY)
            dz += self.dz * dt
        # collisions
        x, y, z = self.position
        x, y, z = self.collide((x + dx, y + dy, z + dz), PLAYER_HEIGHT)
        self.position = (x, y, z)

    def join_message(self):
        x, y, z = self.position
        message = messages.PlayerJoin()
        message.name.value = self.name
        message.x.value = x
        message.y.value = y
        message.o_id.value = self.id
        return message

