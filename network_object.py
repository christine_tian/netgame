from collections import deque
import random
import math
import time
import pyglet
import messages

class PhysicalObject(pyglet.sprite.Sprite):
    def __init__(self, *args, **kwargs):
        super(PhysicalObject, self).__init__(*args, **kwargs)

class NetworkObject(object):

    def __init__(self, batch=None, id=0):

        # Simple function queue implementation. The queue is populated with
        self.queue = deque()

        #self._initialize()

        self.position = (None, None, None)
        self.velocity = (0.0, 0.0)
        self.has_change = False
        self.id = str(id)
        self.client = None

    def update_position(self):
        pass

    def update(self, dt):
        self.update_position()

    def hit_test(self, position, vector, max_distance=8):
        """ Line of sight search from current position. If a block is
        intersected it is returned, along with the block previously in the line
        of sight. If no block is found, return None, None.

        Parameters
        ----------
        position : tuple of len 3
            The (x, y, z) position to check visibility from.
        vector : tuple of len 3
            The line of sight vector.
        max_distance : int
            How many blocks away to search for a hit.

        """
        m = 8
        x, y, z = position
        dx, dy, dz = vector
        previous = None
        for _ in xrange(max_distance * m):
            key = normalize((x, y, z))
            if key != previous and key in self.world:
                return key, previous
            previous = key
            x, y, z = x + dx / m, y + dy / m, z + dz / m
        return None, None

    def get_update_message(self):
        x, y, z = self.position
        msg = messages.ObjectUpdate()
        msg.o_id.value = str(self.id)
        msg.x.value = x
        msg.y.value = y
        msg.z.value = z
        msg.o_type.value = str(self.__class__.__name__)
        return msg

    def _enqueue(self, func, *args):
        """ Add `func` to the internal queue.

        """
        self.queue.append((func, args))

    def _dequeue(self):
        """ Pop the top function from the internal queue and call it.

        """
        func, args = self.queue.popleft()
        func(*args)

    def process_queue(self):
        """ Process the entire queue while taking periodic breaks. This allows
        the game loop to run smoothly. The queue contains calls to
        _show_block() and _hide_block() so this method should be called if
        add_block() or remove_block() was called with immediate=False

        """
        start = time.clock()
        while self.queue and time.clock() - start < 1.0 / TICKS_PER_SEC:
            self._dequeue()

    def process_entire_queue(self):
        """ Process the entire queue with no breaks.

        """
        while self.queue:
            self._dequeue()
