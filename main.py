#__version__ = '0.20.'

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.core.window import Window
from random import randint, choice
from math import radians, pi, sin, cos
import kivent_core
import kivent_cymunk
from kivent_core.gameworld import GameWorld
from kivent_core.managers.resource_managers import texture_manager
from kivent_core.systems.renderers import RotateRenderer
from kivent_core.systems.position_systems import PositionSystem2D
from kivent_core.systems.rotate_systems import RotateSystem2D
from kivy.properties import StringProperty, NumericProperty
from kivent_core.systems.gamesystem import GameSystem
from kivy.factory import Factory
from functools import partial
from os.path import dirname, join, abspath
from simplexnoise import scaled_octave_noise_2d
from kivy.utils import platform
import legume
import messages
import math
import time
import sys
from serializer import entity_to_dict
import json
import threading
import Queue

texture_manager.load_atlas(join('assets','background_objects.atlas'))
texture_manager.load_atlas(join('resources', 'dirt.atlas'))
texture_manager.load_atlas(join('resources', 'texture.atlas'))
texture_manager.load_image(join('resources', 'grass2.png'))

def pos_diff(a, b):
    x1, y1 = a
    x2, y2 = b
    return ((x2-x1), (y2-y1))

class KeyboardListener(Widget): #base code taken from Kivy API documentation
    def __init__(self, **kwargs):
        super(KeyboardListener, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(
        self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self.on_keyPress)
        self._keyboard.bind(on_key_up=self.on_keyRelease)

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self.on_keyPress)
        self._keyboard = None

    def on_keyPress(self, keyboard, keycode, text, modifiers):
        self.parent.keyPress(keyboard,keycode)
        print text
        return True

    def on_keyRelease(self, keyboard, keycode):
        self.parent.keyRelease(keyboard,keycode)
        return True

class MovementSystem(GameSystem):

    def get_motion_vector(self, strafe):
        """ Returns the current motion vector indicating the velocity of the
        player.

        Returns
        -------
        vector : tuple of len 3
            Tuple containing the velocity in x, y, and z respectively.

        """
        if any(strafe):
            x = 0 # was self.rotation
            st = math.degrees(math.atan2(*strafe))
            x_angle = math.radians(x + st)
            dx = math.cos(x_angle)
            dy = math.sin(x_angle)
        else:
            dx = 0.0
            dy = 0.0
        return (dx, dy)

    def update(self, dt):
        entities = self.gameworld.entities
        for component in self.components:
            if component is not None:
                entity_id = component.entity_id
                entity = entities[entity_id]
                d = dt * component.speed # distance covered this tick.
                dx, dy = self.get_motion_vector(component.strafe)
                dx, dy = dx * d, dy * d
                x, y = entity.cymunk_physics.body.position
                x, y = x + dx, y + dy,
                #entity.position = (x, y)
                #entity.cymunk_physics.body.position = (x,y)
                vx, vy = entity.cymunk_physics.body.velocity
                if dx == 0:
                    if abs(vx) > 2*d:
                        if vx > 0:
                            dx = -d
                        else:
                            dx = d
                    else:
                        vx = 0

                if dy == 0:
                    if abs(vy) > 2*d:
                        if vy > 0:
                            dy = -d
                        else:
                            dy = d
                    else:
                        vy = 0
                entity.cymunk_physics.body.velocity = (vx+dx,vy+dy)
                for s in entity.cymunk_physics.shapes:
                    self.gameworld.system_manager['cymunk_physics'].space.reindex_shape(s)

Factory.register('MovementSystem', cls=MovementSystem)

class UpdateSystem(GameSystem):

    def update(self, dt):
        entities = self.gameworld.entities
        for component in self.components:
            if component is not None:
                entity_id = component.entity_id
                entity = entities[entity_id]
                if hasattr(entity, 'cymunk_physics') and hasattr(entity,'server'):
                    x, y = entity.cymunk_physics.body.position
                    vx, vy = entity.cymunk_physics.body.velocity
                    old_x, old_y = component.old_pos
                    old_vx, old_vy = component.old_vel
                    if x != old_x or y != old_y or vx != old_vx or vy != old_vy:
                        component.old_pos = (x, y)
                        component.old_vel = (vx, vy)
                        if entity.server.update == 'sent' or entity.server.update == False:
                            entity.server.update = 'ready'


Factory.register('UpdateSystem', cls=UpdateSystem)


class ServerSystem(GameSystem):

    def on_add_system(self):
        self.server = legume.Server()
        self.server.OnConnectRequest += self.join_request_handler
        self.server.OnMessage += self.server_message_handler
        self.start_thread = False
        self.start_thread2 = False
        self.running = False
        self.pqueue = Queue.PriorityQueue()
        self.queue = Queue.Queue()
        self.server.listen((self.gameworld.parent.SERVER_ADDRESS, int(self.gameworld.parent.SERVER_PORT)))
        print('Listening on port %s' % self.gameworld.parent.SERVER_PORT)

    def on_remove_system(self):
        if hasattr(self,'server'):
            self.server.disconnect_all()
            self.server._shutdown_socket()
            self.server = None
            print 'server removed'

    def join_request_handler(self, sender, args):
        # send current game state
        print 'Client joining'
        #ent = self.gameworld.init_entity(create_dict, ['position', 'rotate', 'rotate_renderer'])
        entities = self.gameworld.entities
        playerid = self.gameworld.parent._add_player()
        self.gameworld.parent.players.append(playerid)
        message = messages.NextPlayerID()
        message.entity.value = playerid
        sender.send_reliable_message(message)
        for component in self.components:
            if component is not None and component.entity_id != playerid:
                component.sender = sender

    def server_message_handler(self, sender, msg):
        # process a message
        print 'got message'
        if msg.MessageTypeID == messages.PlayerJoin.MessageTypeID:
            #player joined ok!
            player = Player(self.player_batch,id=msg.o_id.value,name=msg.name.value,)
            player.position = (msg.x.value, msg.y.value, 0)
            player.id = msg.o_id.value
            print player.name + ' Joined'
            self.objects[player.id] = player
            self.player = player
        if msg.MessageTypeID == messages.PlayerKey.MessageTypeID:
            entities = self.gameworld.entities
            entity = entities[msg.entity.value]
            keycode = msg.keycode.value
            print keycode + ' ' + msg.key_type.value
            if msg.key_type.value == 'press':
                if keycode == 'w' and entity.movement.strafe[0] < 1:
                    entity.movement.strafe[0] += 1
                elif keycode == 's' and entity.movement.strafe[0] > -1:
                    entity.movement.strafe[0] -= 1
                elif keycode == 'a' and entity.movement.strafe[1] > -1:
                    entity.movement.strafe[1] -= 1
                elif keycode == 'd' and entity.movement.strafe[1] < 1:
                    entity.movement.strafe[1] += 1
            elif msg.key_type.value == 'release':
                if keycode == 'w':
                    entity.movement.strafe[0] -= 1 and entity.movement.strafe[0] > -0
                elif keycode == 's':
                    entity.movement.strafe[0] += 1 and entity.movement.strafe[0] < 0
                elif keycode == 'a':
                    entity.movement.strafe[1] += 1 and entity.movement.strafe[1] < 0
                elif keycode == 'd':
                    entity.movement.strafe[1] -= 1 and entity.movement.strafe[1] > -0

        if msg.MessageTypeID == messages.PlayerKeyPress.MessageTypeID:
            self.objects[msg.o_id.value]._on_key_press(msg.symbol.value, msg.modifiers.value)

    def update(self, dt):
        if self.server:
            if self.start_thread == True:
                self.start_thread = False
                self.running = True
                self.lock = threading.Lock()
                #self.net_thread = threading.Thread(target=self.server_update)
                #self.net_thread.daemon = True
                #self.net_thread.start()

                print 'thread started'
            if self.server.peercount > 0 and self.start_thread2 == True:
                self.start_thread2 = False
                self.lock2 = threading.Lock()
                self.net_thread2 = threading.Thread(target=self.process_queue)
                self.net_thread2.daemon = True
                self.net_thread2.start()
                print 'thread2 starting'
            if self.server.peercount > 0:
                self.send_updates()
            else:
                self.server.update()

    def server_update(self):
        while self.server:
            self.lock.acquire()
            try:
                self.server.update()
            except:
                pass
            self.lock.release()
            time.sleep(0.001)
        print('Exited go')

    def send_updates(self):

        entities = self.gameworld.entities
        sent = 0
        for component in self.components:
            if component is not None:
                if component.sender is not None:
                    sent += 1
                    message = messages.CreateEntity()
                    message.entity.value = component.entity_id
                    message.position.value = json.dumps(entity_to_dict(entities[component.entity_id]))
                    if component.sender == 'ALL':
                        self.queue.put_nowait(message)
                    else:
                        #component.sender.send_reliable_message(message)
                        self.queue.put_nowait(message)
                    component.sender = None
                if hasattr(component, 'update'):
                    if component.update == 'ready':
                        sent += 1
                        message = messages.UpdateEntity()
                        message.entity.value = component.entity_id
                        entity = entities[component.entity_id]
                        message.x.value, message.y.value = entity.cymunk_physics.body.position
                        #message.r.value = entity.rotate.r
                        message.vx.value, message.vy.value = entity.cymunk_physics.body.velocity
                        if component.entity_id in self.gameworld.parent.players:
                            self.pqueue.put_nowait((1,message))
                        else:
                            self.queue.put_nowait(message)
                        component.update = 'queued'

    def process_queue(self):
        while self.server:
            try:
                priority,message = self.pqueue.get_nowait()
                self.server.send_reliable_message_to_all(message)
                self.gameworld.entities[message.entity.value].server.update = 'sent'
                self.pqueue.task_done()
            except Queue.Empty:
                pass
            try:
                message = self.queue.get_nowait()
                self.server.send_reliable_message_to_all(message)
                self.gameworld.entities[message.entity.value].server.update = 'sent'
                self.server.update()
                self.queue.task_done()
            except Queue.Empty:
                self.server.update()
            #time.sleep(0.001)
        print 'finished quuq'



Factory.register('ServerSystem', cls=ServerSystem)

class ClientSystem(GameSystem):

    def on_add_system(self):
        self.client = legume.Client()
        self.client.OnConnectRequestAccepted += self.on_connected
        self.client.OnMessage += self.client_message_handler
        self.client.connect((self.gameworld.parent.SERVER_ADDRESS, self.gameworld.parent.SERVER_PORT))
        print('Connecting to %s:%s' % (self.gameworld.parent.SERVER_ADDRESS,self.gameworld.parent.SERVER_PORT))

    def on_remove_system(self):
        if hasattr(self,'client'):
            self.client.disconnect()
            self.client = None
            print 'client disconnecting'

    def on_connected(self, sender, args):
        #create and send player
        print 'Connected to server'
        #sender.send_reliable_message(self.player.join_message())
        #message = messages.PlayerJoin()
        #message.o_id = len(self.objects)
        #self.client.send_reliable_message(message)

    def client_message_handler(self, sender, msg):
        # process a message
        if msg.MessageTypeID == messages.CreateEntity.MessageTypeID:
            ent_dict = json.loads(msg.position.value)
            create_order = ['position', 'rotate']
            pos = (ent_dict['position'][0],ent_dict['position'][1])
            rotate = ent_dict['rotate']['r']
            #for item in ent_dict['load_order']:
            #    create_order.append(str(item))
            if 'rotate_renderer' in ent_dict:
                create_dict = {
                            'position': pos,
                            'rotate': 0,
                            'rotate_renderer': {'texture': str(ent_dict['rotate_renderer']['texture']), 'size': (50., 50.), 'render':True},
                        }
                create_order.append('rotate_renderer')
            if 'renderer' in ent_dict:
                create_dict = {
                            'position': pos,
                            'rotate': 0,
                            'renderer': {'texture': str(ent_dict['renderer']['texture']), 'size': (50., 50.), 'render':True},
                        }
                create_order.append('renderer')

            if 'cymunk_physics' in ent_dict:
                bd = {'velocity': (ent_dict['cymunk_physics']['body']['velocity'][0],ent_dict['cymunk_physics']['body']['velocity'][1]),
                          'position': (ent_dict['cymunk_physics']['body']['position'][0], ent_dict['cymunk_physics']['body']['position'][1]),
                          'angle': ent_dict['cymunk_physics']['body']['angle'],
                          'angular_velocity': ent_dict['cymunk_physics']['body']['angular_velocity'],
                          'vel_limit': ent_dict['cymunk_physics']['body']['vel_limit'],
                          'ang_vel_limit': ent_dict['cymunk_physics']['body']['ang_vel_limit'],
                          'mass': ent_dict['cymunk_physics']['body']['mass']
                    }
                shapes = []
                for s in ent_dict['cymunk_physics']['shapes']:
                    if s['shape_type'] == 'Circle':
                        shape_dict = {'inner_radius': 0, 'outer_radius': s['shape_info']['outer_radius'], 'mass': s['shape_info']['mass'], 'offset': (s['shape_info']['offset'][0], s['shape_info']['offset'][1])}
                    if s['shape_type'] == 'BoxShape':
                        shape_dict = {'width': 50, 'height': 50, 'mass': s['shape_info']['mass']}
                    sd = {'collision_type': int(s['collision_type']), 'elasticity': s['elasticity'], 'friction': s['friction'], 'shape_type': str(ent_dict['cymunk_physics']['shape_type']), 'shape_info': shape_dict}
                    shapes.append(sd)
                create_dict['cymunk_physics'] = {'main_shape': ent_dict['cymunk_physics']['shape_type'],
                                                 'velocity': bd['velocity'],
                                                 'position': bd['position'],
                                                 'angle': float(bd['angle']),
                                                 'angular_velocity': float(bd['angular_velocity']),
                                                 'vel_limit': float(bd['vel_limit']),
                                                 'ang_vel_limit': float(bd['ang_vel_limit']),
                                                 'mass': float(bd['mass']),
                                                 'col_shapes':shapes}
                create_order.append('cymunk_physics')

            ent_id = self.gameworld.init_entity(create_dict, create_order)
            self.gameworld.CtoSid[ent_id] = msg.entity.value
            self.gameworld.StoCid[msg.entity.value] = ent_id
        if msg.MessageTypeID == messages.NextPlayerID.MessageTypeID:
            print 'Server player id is ' + str(msg.entity.value)
            playerid = self.gameworld.parent._add_player()
            self.gameworld.CtoSid[playerid] = msg.entity.value
            self.gameworld.StoCid[msg.entity.value] = playerid
            self.gameworld.parent.playerID = playerid
            self.gameworld.parent.player = self.gameworld.entities[playerid]
            self.gameworld.system_manager['default_gameview'].focus_entity = False
            self.gameworld.system_manager['default_gameview'].entity_to_focus = playerid
            if platform != 'android':
                self.gameworld.parent.add_widget(KeyboardListener())
            self.gameworld.system_manager['cymunk_physics'].add_collision_handler(1,2,begin_func=self.gameworld.parent.player_collide)
        if msg.MessageTypeID == messages.UpdateEntity.MessageTypeID:

            try:
                entity = self.gameworld.entities[self.gameworld.StoCid[msg.entity.value]]
                pos = (msg.x.value, msg.y.value)
                vel = (msg.vx.value, msg.vy.value)
                if hasattr(entity,'cymunk_physics'):
                    dx, dy = pos_diff(entity.cymunk_physics.body.position, pos)
                    vx,vy = vel
                    #if abs(dx) > 5 or abs(dy) > 5:
                    #    entity.cymunk_physics.body.position = pos
                    #else:
                    #    if abs(dx) > 1:
                    #        vx += dx
                    #
                    #    if abs(dy) > 1:
                    #        vy += dy
                    print "diff is " + str(dx) +','+ str(dy)
                    #entity.position.x = pos[0]
                    #entity.position.y = pos[1]
                    #entity.cymunk_physics.body.position = (pos[0],pos[1])
                    entity.cymunk_physics.body.velocity = (vx+dx/2,vy+dy/2)
                    for s in entity.cymunk_physics.shapes:
                        self.gameworld.system_manager['cymunk_physics'].space.reindex_shape(s)
            except KeyError:
                print 'not item yet'


    def update(self, dt):
        if self.client:
            self.client.update()
            entities = self.gameworld.entities
            for component in self.components:
                if component is not None:
                    entity_id = component.entity_id
                    entity = entities[entity_id]
                    message = messages.PlayerKey()
                    message.keycode.value = component.keycode[1]
                    message.key_type.value = component.key_type
                    message.entity.value = self.gameworld.CtoSid[self.gameworld.parent.playerID]
                    self.client.send_reliable_message(message)
                    self.gameworld.remove_entity(entity_id)

Factory.register('ClientSystem', cls=ClientSystem)

class TestGame(Widget):
    def __init__(self, **kwargs):
        super(TestGame, self).__init__(**kwargs)
        self.gameworld.init_gameworld(
            ['position', 'rotate','cymunk_physics', 'renderer','rotate_renderer','cymunk_touch', 'default_gameview','map'],
            callback=self.init_game)
        self.playerID = None
        self.player = None
        self.players = []
        self.SERVER = False
        self.SERVER_PORT = 10001
        self.SERVER_ADDRESS = '127.0.0.1'
        self.gameworld.CtoSid = {}
        self.gameworld.StoCid= {}
        Clock.schedule_interval(self.update, 0)


    def init_game(self):
        self.setup_states()
        self.set_state()
        #self.setup_map()
        self.space = self.gameworld.system_manager['cymunk_physics'].space

    def keyPress(self,keyboard,keycode):

        if self.SERVER != True:
            create_dict = {
                'client': {'keycode': keycode, 'key_type': 'press'},
            }
            self.gameworld.init_entity(create_dict, ['client'])
        else:

            if keycode[1] == 'w' and self.player.movement.strafe[0] < 1:
                self.player.movement.strafe[0] += 1
            elif keycode[1] == 's' and self.player.movement.strafe[0] > -1:
                self.player.movement.strafe[0] -= 1
            elif keycode[1] == 'a' and self.player.movement.strafe[1] > -1:
                self.player.movement.strafe[1] -= 1
            elif keycode[1] == 'd' and self.player.movement.strafe[1] < 1:
                self.player.movement.strafe[1] += 1

    def keyRelease(self,keyboard,keycode):

        if self.SERVER != True:
            create_dict = {
                'client': {'keycode': keycode, 'key_type': 'release'},
            }
            self.gameworld.init_entity(create_dict, ['client'])
        else:
            if keycode[1] == 'w':
                self.player.movement.strafe[0] -= 1 and self.player.movement.strafe[0] > -0
            elif keycode[1] == 's':
                self.player.movement.strafe[0] += 1 and self.player.movement.strafe[0] < 0
            elif keycode[1] == 'a':
                self.player.movement.strafe[1] += 1 and self.player.movement.strafe[1] < 0
            elif keycode[1] == 'd':
                self.player.movement.strafe[1] -= 1 and self.player.movement.strafe[1] > -0

    def destroy_created_entity(self, ent_id, dt):
        self.gameworld.remove_entity(ent_id)
        self.app.count -= 1

    def draw_some_stuff(self):
        gameview = self.gameworld.system_manager['default_gameview']
        x, y = int(-gameview.camera_pos[0]), int(-gameview.camera_pos[1])
        w, h =  int(gameview.size[0] + x), int(gameview.size[1] + y)
        delete_time = 2.5
        create_asteroid = self.create_asteroid
        destroy_ent = self.destroy_created_entity
        for z in range(10):

            pos = (randint(x, w), randint(y, h))
            ent_id = create_asteroid(pos)
            #Clock.schedule_once(partial(destroy_ent, ent_id), delete_time)
        self.app.count += 100

    def same_material(self, mat, material):
        if mat == material:
            return 1
        return 0

    def add_background(self):

        size = (32,32)
        xsize, ysize = size
        octaves = 4
        tiles={}
        freq = 1*octaves

        for x in range(-xsize,xsize):
            row = ''
            for y in range(-ysize,ysize):
                #number = int(snoise2(x / freq, y / freq, octaves)*50)+50
                #number = int(randint(0,100))
                number = scaled_octave_noise_2d(3, .2, .05, 0, 100, x, y)
                #row += str(number)
                if number > 50:
                    material = 'dirt'
                    row += '#'
                else:
                    material = 'grass'
                    row += '_'
                #row +=','
                tile = {'position':(x,y), 'material':material}
                tiles[(x,y)] = tile
            print row

        dirt = []
        for x in range(0,16):
            dirt.append('dirt'+str(x))
        for x in range(-xsize,xsize):
            for y in range(-ysize,ysize):
                edge = False
                if x == xsize-1 or x == -xsize or y == ysize-1 or y == -ysize:
                    edge = True
                pos = x*50., y*50.
                if edge:
                    shape_dict = {'width': 50, 'height': 50,
                        'mass': 0.0}
                    col_shape = {'shape_type': 'box', 'elasticity': .5,
                        'collision_type': 2, 'shape_info': shape_dict, 'friction': 1.0}
                    col_shapes = [col_shape]
                    physics_component = {'main_shape': 'box',
                        'velocity': (0, 0),
                        'position': pos, 'angle': 0,
                        'angular_velocity': 0,
                        'vel_limit': 1000,
                        'ang_vel_limit': radians(200),
                        'mass': 0.0, 'col_shapes': col_shapes}
                    create_dict = {'cymunk_physics': physics_component,
                        'renderer': {'texture': 'brick',
                        'size': (50, 50),
                        'render': True},
                        'position': pos,
                        'rotate': 0,
                        'server': {'sender': None},
                    }
                    create_order = ['position', 'rotate', 'renderer',
                        'cymunk_physics', 'server']
                else:
                    create_dict = {
                        'position': pos,
                        'rotate': 0,
                        'renderer': {'texture': 'grass2', 'size': (50., 50.), 'render':True},
                        'server': {'sender': None},
                    }
                    create_order = ['position', 'rotate', 'renderer','server']
                ent = self.gameworld.init_entity(create_dict, create_order)

        for x in range(-xsize+1,xsize-1):
            for y in range(-ysize+1,ysize-1):
                #create sprites for world
                mat = tiles[(x,y)]['material']
                if mat == 'dirt':
                    index = 0
                    index += self.same_material(mat,tiles[(x,y+1)]['material'])*1
                    index += self.same_material(mat, tiles[(x+1,y)]['material'])*2
                    index += self.same_material(mat,tiles[(x,y-1)]['material'])*4
                    index += self.same_material(mat,tiles[(x-1,y)]['material'])*8
                    pos = x*50., y*50.
                    create_dict = {
                        'position': pos,
                        'rotate': 0,
                        'renderer': {'texture': 'dirt'+str(index), 'size': (50., 50.), 'render':True},
                        'server': {'sender': None},
                    }
                    ent = self.gameworld.init_entity(create_dict, ['position', 'rotate', 'renderer','server'])
                #self.tiles[(x,y)].add_img(index)
                #self.tiles[(x,y)].update_position()

    def add_server(self,ip):
        print ip
        self.SERVER = True
        self.SERVER_ADDRESS = ip
        self.gameworld.state='server'
        if self.SERVER == True:
            self.playerID = self._add_player()
            self.player = self.gameworld.entities[self.playerID]
            self.players.append(self.playerID)
            self.gameworld.system_manager['default_gameview'].focus_entity = False
            self.gameworld.system_manager['default_gameview'].entity_to_focus = self.playerID
            if platform != 'android':
                self.add_widget(KeyboardListener())
            self.gameworld.system_manager['cymunk_physics'].add_collision_handler(1,2,begin_func=self.player_collide)
        if self.SERVER == True:
            self.add_background()
        self.gameworld.system_manager['server'].start_thread = True
        self.gameworld.system_manager['server'].start_thread2 = True

    def add_client(self,ip):
        self.SERVER_ADDRESS = ip
        self.gameworld.state='server'

    def _add_player(self):
        gameview = self.gameworld.system_manager['default_gameview']
        x, y = int(-gameview.camera_pos[0]), int(-gameview.camera_pos[1])
        w, h =  int(gameview.size[0]), int(gameview.size[1])
        pos = (x+w/2,y+h/2)
        shape_dict = {'inner_radius': 0, 'outer_radius': 20,
            'mass': 50.0, 'offset': (0, 0)}
        col_shape = {'shape_type': 'circle', 'elasticity': .5,
            'collision_type': 1, 'shape_info': shape_dict, 'friction': 1.0}
        col_shapes = [col_shape]
        physics_component = {'main_shape': 'circle',
            'velocity': (0, 0),
            'position': pos, 'angle': 0,
            'angular_velocity': 0,
            'vel_limit': 1000,
            'ang_vel_limit': radians(200),
            'mass': 50.0, 'col_shapes': col_shapes}
            #'client': {'client': True, 'keycode': None, 'key_type': None, 'update': False}}
        if self.SERVER == True:
            create_component_dict = {'cymunk_physics': physics_component,
            'rotate_renderer': {'texture': 'star5',
            'size': (45, 45),
            'render': True},
            'position': pos,
            'rotate': 0,
            'movement': {'strafe':[0,0],'speed':100},
            'server': {'sender': None, 'update':False},
            'update': {'old_pos':pos, 'old_vel':(0,0)}}
            component_order = ['position', 'rotate', 'rotate_renderer','cymunk_physics','movement','server','update']
            print 'created with server'
        else:
            create_component_dict = {'cymunk_physics': physics_component,
            'rotate_renderer': {'texture': 'star5',
            'size': (45, 45),
            'render': True},
            'position': pos,
            'rotate': 0,
            'movement': {'strafe':[0,0],'speed':100},}
            component_order = ['position', 'rotate', 'rotate_renderer','cymunk_physics', ]
        self.playerID = self.gameworld.init_entity(
            create_component_dict, component_order)
        return self.playerID


    def player_collide(self, space, arbiter):
        print 'collision!'
        print arbiter.shapes[0].body.data
        other = arbiter.shapes[1].body.data
        print self.playerID
        Clock.schedule_once(partial(self.destroy_created_entity, other), .01)
        return True

    def create_asteroid(self, pos):
        x_vel = randint(-500, 500)
        y_vel = randint(-500, 500)
        angle = radians(randint(-360, 360))
        angular_velocity = radians(randint(-150, -150))
        shape_dict = {'inner_radius': 0, 'outer_radius': 20,
            'mass': 50, 'offset': (0, 0)}
        col_shape = {'shape_type': 'circle', 'elasticity': .5,
            'collision_type': 3, 'shape_info': shape_dict, 'friction': 1.0}
        col_shapes = [col_shape]
        physics_component = {'main_shape': 'circle',
            'velocity': (x_vel, y_vel),
            'position': pos, 'angle': angle,
            'angular_velocity': angular_velocity,
            'vel_limit': 1000,
            'ang_vel_limit': radians(200),
            'mass': 50, 'col_shapes': col_shapes}
        create_component_dict = {'cymunk_physics': physics_component,
            'rotate_renderer': {'texture': 'asteroid1',
            'size': (45, 45),
            'render': True},
            'position': pos, 'rotate': 0,
            'server': {'sender': 'ALL', 'update': False},
            'update': {'old_pos':pos, 'old_vel':(0,0)}}
        component_order = ['position', 'rotate', 'rotate_renderer','cymunk_physics','server','update']
        return self.gameworld.init_entity(
            create_component_dict, component_order)

    def update(self, dt):
        #if(self.playerID):
        #    player = self.gameworld.entities[self.playerID]
        #    player.cymunk_physics.body.position = (100,100)
        #    player.cymunk_physics.body.angle = 0.0
        #    for s in player.cymunk_physics.shapes:
        #        self.space.reindex_shape(s)

        self.gameworld.update(dt)

    def setup_states(self):
        self.gameworld.add_state(state_name='main',
            systems_added=['rotate_renderer','map','renderer'],
            systems_removed=['server','client'], systems_paused=[],
            systems_unpaused=['rotate_renderer','map','renderer'],
            screenmanager_screen='main')
        self.gameworld.add_state(state_name='server',
            systems_added=['server','client','update'],
            systems_removed=[], systems_paused=[],
            systems_unpaused=['server','client'],
            screenmanager_screen='server',
            on_change_callback=self.add_server_system)

    def add_server_system(self,current,last):
        if current == 'server':
            self.SERVER_PORT = 10000
            if self.SERVER == True:
                self.gameworld.system_manager['server'].on_add_system()
            else:
                self.gameworld.system_manager['client'].on_add_system()

    def set_state(self):
        self.gameworld.state = 'main'


class DebugPanel(Widget):
    fps = StringProperty(None)

    def __init__(self, **kwargs):
        super(DebugPanel, self).__init__(**kwargs)
        Clock.schedule_once(self.update_fps)

    def update_fps(self,dt):
        self.fps = str(int(Clock.get_fps()))
        Clock.schedule_once(self.update_fps, .05)

class NetGameApp(App):
    count = NumericProperty(0)

if __name__ == '__main__':
    NetGameApp().run()
