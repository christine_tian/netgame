import pyglet

pyglet.resource.path= ['resources']
pyglet.resource.reindex()
from pyglet.gl import *


player_image = pyglet.resource.image("man-ne.png").get_texture()
start_server = pyglet.resource.image("start_server.jpg")
start_client = pyglet.resource.image("start_client.jpg")
dirt = pyglet.resource.image("dirt.png")
grass = pyglet.resource.image("grass2.png").get_texture()
glTexParameteri(grass.target, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
glTexParameteri(grass.target, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
texture = pyglet.resource.image('texture.png').get_texture()
dirt16 = pyglet.resource.image('dirt16.png').get_texture()
glTexParameteri(dirt16.target, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
glTexParameteri(dirt16.target, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
glTexParameteri(texture.target, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
glTexParameteri(texture.target, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

background = pyglet.graphics.OrderedGroup(0)
foreground = pyglet.graphics.OrderedGroup(1)

dirt_images= []
for i in range(0,16):
    dirt_images.append(dirt16.get_region(i*64,0,64,64))

dirt_texture = texture.get_region(0,64,64,64)


class Rectangle(object):
    '''Draws a rectangle into a batch.'''
    def __init__(self, x1, y1, x2, y2, batch):
        self.vertex_list = batch.add(4, pyglet.gl.GL_QUADS, None,
            ('v2i', [x1, y1, x2, y1, x2, y2, x1, y2]),
            ('c4B', [200, 200, 220, 255] * 4)
        )

class TextWidget(object):
    def __init__(self, text, x, y, width, batch):
        self.to_delete = False
        self.document = pyglet.text.document.UnformattedDocument(text)
        self.document.set_style(0, len(self.document.text),
            dict(color=(0, 0, 0, 255))
        )
        font = self.document.get_font()
        height = font.ascent - font.descent
        self.height = height
        self.layout = pyglet.text.layout.IncrementalTextLayout(
            self.document, width, height, multiline=False, batch=batch)
        self.caret = pyglet.text.caret.Caret(self.layout)

        self.layout.x = x
        self.layout.y = y

        # Rectangular outline
        pad = 2
        self.pad = pad
        self.rectangle = Rectangle(x - pad, y - pad, x + width + pad, y + height + pad, batch)

    def hit_test(self, x, y):
        return (0 < x - self.layout.x < self.layout.width and
                0 < y - self.layout.y < self.layout.height)

    def delete(self):
        self.layout.delete()
        self.rectangle.vertex_list.delete()

    def on_resize(self, width, height):
        self.rectangle.vertex_list.delete()
        self.rectangle = Rectangle(self.layout.x - self.pad, self.layout.y - self.pad, self.layout.x + width + self.pad-200, self.layout.y + self.height + self.pad, self.layout.batch)

class ButtonWidget(object):
    def __init__(self, image, x, y, batch):
        self.batch = batch
        self.sprite = pyglet.sprite.Sprite(img=image, x=x, y=y, batch=self.batch)
        self.clicked = False
        self.to_delete = False

    def hit_test(self, x, y):
        return (0 < x - self.sprite.x < self.sprite.width and
                0 < y - self.sprite.y < self.sprite.height)

    def on_resize(self, width, height):
        pass

    def on_clicked(self):
        self.clicked = True

    def delete(self):
        self.sprite.delete()
